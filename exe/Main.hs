module Main where

import qualified MyLib (oledFuncAnim)

main :: IO ()
main = do
  MyLib.oledFuncAnim "x%(y+1)"
