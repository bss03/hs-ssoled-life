{-# LANGUAGE DeriveFunctor #-}

module MyLib (oledFuncDraw, oledFuncAnim, extendArrayVoid, extendArrayWhiteRoom) where

-- base
import Control.Applicative (empty, liftA2, (<|>))
import Control.Monad (ap, join, void)
import Control.Monad.Combinators (between)
import Control.Monad.Combinators.Expr (Operator (InfixN), makeExprParser)
import Data.Array (Array, Ix, bounds, inRange, listArray, range, (!))
import Data.Bits (Bits (unsafeShiftL, unsafeShiftR, (.&.), (.|.)), xor)
import Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import Data.Char (isAlpha)
import Data.Fix (Fix (Fix), foldFix)
import Data.Void (Void)
import System.HIDAPI (ProductID, VendorID, sendFeatureReport)
import qualified System.HIDAPI as HID
import Text.Megaparsec (Parsec, eof, notFollowedBy, oneOf, parse, single, takeWhile1P, try)
import Text.Megaparsec.Char (hspace, string)
import Text.Megaparsec.Char.Lexer (decimal, signed)

(.<<.) :: Bits a => a -> Int -> a
(.<<.) = unsafeShiftL

(.>>.) :: Bits a => a -> Int -> a
(.>>.) = unsafeShiftR

steelseries :: VendorID
steelseries = 0x1038

ssApex7 :: ProductID
ssApex7 = 0x1612

setOledImage :: Num a => a
setOledImage = 0x61

reifyMatrix :: (Ix x, Ix y) => (x -> y -> z) -> ((x, y), (x, y)) -> Array (x, y) z
reifyMatrix f bs = listArray bs . map (uncurry f) $ range bs

conwayLifeStep :: (Enum x, Ix x, Enum y, Ix y) => (x -> y -> Bool) -> ((x, y), (x, y)) -> Array (x, y) Bool
conwayLifeStep old = reifyMatrix c
  where
    c x y = neighborhoodCount == 3 || live && neighborhoodCount == 4
      where
        live = old x y
        neighborhoodCount =
          sum . map (fromEnum . uncurry old) $ range ((pred x, pred y), (succ x, succ y))

ssa7Height :: Num a => a
ssa7Height = 40

ssa7Width :: Num a => a
ssa7Width = 128

ssa7Bounds :: (Num x, Num y) => ((x, y), (x, y))
ssa7Bounds = ((0, 0), (ssa7Width - 1, ssa7Height - 1))

extendArrayConstant :: Ix index => value -> Array index value -> index -> value
extendArrayConstant _ arr ix | inRange (bounds arr) ix = arr ! ix
extendArrayConstant x _ _ = x

extendArrayVoid :: Ix index => Array index Bool -> index -> Bool
extendArrayVoid = extendArrayConstant False

extendArrayWhiteRoom :: Ix index => Array index Bool -> index -> Bool
extendArrayWhiteRoom = extendArrayConstant True

extendArrayTorus :: (Ix x, Integral x, Ix y, Integral y) => Array (x, y) value -> x -> y -> value
extendArrayTorus arr x y = arr ! (x', y')
  where
    x' = dx + ((x - dx) `mod` w)
    y' = dy + ((y - dy) `mod` h)
    ((dx, dy), (mx, my)) = bounds arr
    w = mx - dx + 1
    h = my - dy + 1

-- |
-- 'conwayLifeStep' specialized to the SteelSeries Apex 7 OLED (cells outside the bounds are
-- dead).
ss7CLS :: (Int -> Int -> Bool) -> Array (Int, Int) Bool
ss7CLS f = conwayLifeStep f ssa7Bounds

-- | Generates packed data, width must be divisive by 8; like for the SteelSeries Apex 7
fImagedata :: (Int -> Int -> Bool) -> ByteString
fImagedata f = BS.pack [fb x y | y <- take ssa7Height [0 ..], x <- take (ssa7Width .>>. 3) [0 ..]]
  where
    fb x y =
      foldl1 (.|.) [if f ((x .<<. 3) .|. z) y then (1 .<<. 7) .>>. z else 0 | z <- take (1 .<<. 3) [0 ..]]

data ExprF r
  = ConstExpr Int
  | X
  | Y
  | BinOp (Int -> Int -> Int) r r
  deriving (Functor)

data Op
  = ConstOp Int
  | IdX
  | IdY
  | Fx (Int -> Int)
  | Fy (Int -> Int)
  | Fxy (Int -> Int -> Int)

exprOpAlg :: ExprF Op -> Op
exprOpAlg (ConstExpr i) = ConstOp i
exprOpAlg X = IdX
exprOpAlg Y = IdY
exprOpAlg (BinOp f (ConstOp i) (ConstOp j)) = ConstOp $ f i j
exprOpAlg (BinOp f (ConstOp i) IdX) = Fx $ f i
exprOpAlg (BinOp f (ConstOp i) IdY) = Fy $ f i
exprOpAlg (BinOp f (ConstOp i) (Fx h)) = Fx $ f i . h
exprOpAlg (BinOp f (ConstOp i) (Fy h)) = Fy $ f i . h
exprOpAlg (BinOp f (ConstOp i) (Fxy h)) = Fxy $ (f i .) . h
exprOpAlg (BinOp f IdX (ConstOp j)) = Fx $ flip f j
exprOpAlg (BinOp f IdX IdX) = Fx $ join f
exprOpAlg (BinOp f IdX IdY) = Fxy f
exprOpAlg (BinOp f IdX (Fx h)) = Fx $ f <*> h
exprOpAlg (BinOp f IdX (Fy h)) = Fxy $ (. h) . f
exprOpAlg (BinOp f IdX (Fxy h)) = Fxy $ liftA2 (.) f h
exprOpAlg (BinOp f IdY (ConstOp j)) = Fy $ flip f j
exprOpAlg (BinOp f IdY IdX) = Fxy $ flip f
exprOpAlg (BinOp f IdY IdY) = Fy $ join f
exprOpAlg (BinOp f IdY (Fx h)) = Fxy $ flip f . h
exprOpAlg (BinOp f IdY (Fy h)) = Fy $ f <*> h
exprOpAlg (BinOp f IdY (Fxy h)) = Fxy $ ap f . h
exprOpAlg (BinOp f (Fx g) (ConstOp j)) = Fx $ flip f j . g
exprOpAlg (BinOp f (Fx g) IdX) = Fx $ f =<< g
exprOpAlg (BinOp f (Fx g) IdY) = Fxy $ f . g
exprOpAlg (BinOp f (Fx g) (Fx h)) = Fx $ liftA2 f g h
exprOpAlg (BinOp f (Fx g) (Fy h)) = Fxy $ (. h) . f . g
exprOpAlg (BinOp f (Fx g) (Fxy h)) = Fxy $ (.) . f . g <*> h
exprOpAlg (BinOp f (Fy g) (ConstOp j)) = Fy $ flip f j . g
exprOpAlg (BinOp f (Fy g) IdX) = Fxy $ flip (f . g)
exprOpAlg (BinOp f (Fy g) IdY) = Fy $ f =<< g
exprOpAlg (BinOp f (Fy g) (Fx h)) = Fxy $ flip (f . g) . h
exprOpAlg (BinOp f (Fy g) (Fy h)) = Fy $ liftA2 f g h
exprOpAlg (BinOp f (Fy g) (Fxy h)) = Fxy $ liftA2 f g . h
exprOpAlg (BinOp f (Fxy g) (ConstOp j)) = Fxy $ flip flip j . (f .) . g
exprOpAlg (BinOp f (Fxy g) IdX) = Fxy $ flip =<< (f .) . g
exprOpAlg (BinOp f (Fxy g) IdY) = Fxy $ join . (f .) . g
exprOpAlg (BinOp f (Fxy g) (Fx h)) = Fxy $ flip . (f .) . g <*> h
exprOpAlg (BinOp f (Fxy g) (Fy h)) = Fxy $ (<*> h) . (f .) . g
exprOpAlg (BinOp f (Fxy g) (Fxy h)) = Fxy $ ap . (f .) . g <*> h

opFn :: Op -> Int -> Int -> Bool
opFn = f
  where
    f (ConstOp i) = const . const $ intBool i
    f IdX = const . intBool
    f IdY = const $ intBool
    f (Fx fx) = const . intBool . fx
    f (Fy fy) = const $ intBool . fy
    f (Fxy fxy) = (intBool .) . fxy
    intBool = (0 ==)

type Expr = Fix ExprF

parseExpr :: Parsec Void String Expr
parseExpr = makeExprParser term [ops]
  where
    term = (between (single '(') (single ')') parseExpr <|> var <|> try int) <* hspace
    int = Fix . ConstExpr <$> signed (pure ()) decimal
    var = do
      name <- takeWhile1P (Just "letter") isAlpha
      case name of
        "x" -> return $ Fix X
        "y" -> return $ Fix Y
        "X" -> return $ Fix X
        "Y" -> return $ Fix Y
        _ -> empty
    ops = iops ++ bops
    iops =
      map
        (\(f, s) -> InfixN $ (Fix .) . BinOp f <$ try (string s <* notFollowedBy (oneOf "+-*/%&|^") <* hspace))
        [ ((+), "+"),
          ((-), "-"),
          ((*), "*"),
          (quot, "/"),
          (rem, "%"),
          ((^), "**"),
          ((.&.), "&"),
          ((.|.), "|"),
          (xor, "^")
        ]
    bops =
      map
        (\(f, s) -> InfixN $ (Fix .) . BinOp ((fromEnum .) . f) <$ try (string s <* notFollowedBy (oneOf "<=>!") <* hspace))
        [ ((<), "<"),
          ((<=), "<="),
          ((>), ">"),
          ((>=), ">="),
          ((==), "=="),
          ((/=), "!="),
          ((==), "==="),
          ((/=), "!==")
        ]

runExpr :: Expr -> Int -> Int -> Bool
runExpr = opFn . foldFix exprOpAlg

reportdata :: Expr -> ByteString
reportdata expr = BS.append (fImagedata $ runExpr expr) (BS.singleton 0)

onFirstSS7 :: (HID.DeviceInfo -> IO a) -> IO ()
onFirstSS7 f = do
  devinfo <- findSteelSeriesApex7
  case devinfo of
    [] -> putStrLn "No SteelSeries Apex 7 found."
    (h : _) -> void $ f h

findSteelSeriesApex7 :: IO [HID.DeviceInfo]
findSteelSeriesApex7 =
  filter ((1 ==) . HID.interfaceNumber) <$> HID.enumerate (Just steelseries) (Just ssApex7)

oledFuncDraw :: String -> IO ()
oledFuncDraw exprStr = do
  onFirstSS7 $ \h ->
    case parse (parseExpr <* eof) "expr" exprStr of
      Left errBdl -> print errBdl
      Right expr -> do
        ss <- HID.openDeviceInfo h
        _err <- sendFeatureReport ss setOledImage (reportdata expr)
        HID.close ss

oledFuncAnim :: String -> IO ()
oledFuncAnim exprStr = case pExpr of
  Left err -> print err
  Right expr -> onFirstSS7 $ \h -> do
    ss <- HID.openDeviceInfo h
    let loop arr n = do
          _err <- sendFeatureReport ss setOledImage (BS.snoc (fImagedata $ curry (arr !)) 0)
          -- threadDelay 1
          let next = ss7CLS (extendArrayTorus arr)
          if n <= 0 then pure () else loop next (pred n)
    loop (reifyMatrix (runExpr expr) ssa7Bounds) (1000 :: Int)
    HID.close ss
  where
    pExpr = parse (parseExpr <* eof) "expr" exprStr
